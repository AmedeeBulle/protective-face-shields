## 3- Flexible headband solution - a 70cm lanyard

This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital (Belgium), and shared under a [Creative Commons license BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).

This version has been slightly adapted thanks to feedback from the CHU Saint-Pierre hospital in Brussels. It is very fast to produce and less brittle than the rigid version. It is also shared under a [Creative Commons license BY-NC-SA 3.0](https://creativecommons.org/licenses/by-nc-sa/3.0/fr/).

![](../images/headband-flexible-general.png)

#### Hardware

* Material
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 0.8 mm thick, 70 cm long minimum.
* Tools:
  * Laser cutting machine
  * Cutter
* Production time: 2 min/piece


#### Manufacturing process

### Step 1 - Cutting out the structure ("strip" below)
Download the template below (.SLDPRT or .DXF) and perform cutting.

* [stripes.sldprt](files/Bandeau-masque-anti-projection-700.SLDPRT)
* [stripes.dxf](files/Bandeau-masque-anti-projection-700.DXF)




### Step 2 - Cut with the cutter the transparent A4 sheet of paper

**This step must be performed using the cutter and not the laser cutting machine.**

Cut out from the transparent sheet, 6 notches 22mm long located as shown in the drawing below (in mm). It has been found that an adjusted notch (with dimensions equal to the width of the strips) is necessary for a better hold of the transparent sheet.

![](../PFC-Headband-Flexible-LaserCut/images/flexible-70cm-cuts.jpg)

To simplify cutting and assembly, a version with 4 notches is also available to ensure that the mask is held securely in place:

![](../PFC-Headband-Flexible-LaserCut/images/flexible-70cm-cuts2.jpg)

### Step 3 - Assemble

The assembly is done at the hospital, but is described for information:

1. Insert the strip into the notches along the entire length of the sheet.

![](../PFC-Headband-Flexible-LaserCut/images/flexible-70cm-assembly1.png)


2. Remove the central part of the strip from the 2 central notches of the transparent sheet.

![](../PFC-Headband-Flexible-LaserCut/images/flexible-70cm-assembly2.png)

3. Insert the small slat into the notch to adjust the distance between the front and the transparent sheet.


![](../PFC-Headband-Flexible-LaserCut/images/flexible-70cm-assembly3.png)

## Authors and Acknowledgements

Tutorial written by Orianne Bastin, François Huberland, Loïc Blanc (BEAMS & Fablab ULB).  Thanks to the whole team for your contribution and your pictures.
