# Protective Face Shields

[Pour le français cliquez ici](index.md)

### How to make Protective Face Shields in Fablabs

A **multidisciplinary** team at the Fablab ULB (Belgium) has developed Face Protective Masks in collaboration with the Hospital CHU Saint-Pierre in Brussels. We are now contacted by several hospitals in Belgium and outside Belgium. We are currently supported by the Fab-C in Charleroi and the Fablab YourLab in Andenne, other Fablabs are joining the operation.

Here below are several tutorials to reproduce these masks in  different fablabs. The 3 versions on this site are validated by the hospital CHU Saint-Pierre in Bruxelles.
For expert makers, [here is our Gitlab repository](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields) with all the source files of the validated masks and the prototypes we are making.


## News

[28 mars] A new tutorial available with a simple hygiene cap

[March 26] To be updated.
100 masks (sol-1 3D printed) are being produced by Andreas Kopp for the hospital Wolfartsklinik Gräfelfing (Germany)
120 masks have been produced and delivered to the CPAS of Wavre.  
50 masks for the Erasmus Hospital

[March 25] 732 masks produced and delivered (620 laser cut, 112 3D printed).  
50 protos produced (new model in preparation).

[March 24] 640 masks produced and delivered (540 laser cut, 100 3D printed).  
4 other prototypes are under development at Fablab ULB to increase production capacity. They are currently being validated by the CHU Saint-Pierre.
[RTBF JT de 13h et de 19h - 4'50"](https://www.rtbf.be/auvio/detail_jt-13h?id=2616606)

[March 23] 140 masks were delivered to the CHU Saint-Pierre in Brussels.

[March 22, 2020] 2 prototypes have been validated by the CHU Saint-Pierre in Brussels.


Translated with www.DeepL.com/Translator (free version)

## Context

##### General need
  Urgent need for several thousand of protective face masks in Brussels hospitals (Belgium). Estimated number: 7500 units in Brussels.

##### Confirmed need :
  * Hospital CHU Saint-Pierre in Brussels -> 1500 units
  * Erasmus Hospital (Brussels)
  * ISPPC - CHU Charleroi


##### Validation process [IMPORTANT]
  In Belgium, each model must be validated by a hospital and a 3-step process before it can be produced in large quantities and used by a hospital :
      1. a hospital physician
      2. a hospital medical officer of health
      3. hospital management

##### Prototyping and validated solutions :
  The Fablab ULB has developed 2 solutions validated by the CHU Saint-Pierre BXL on March 22nd. A solution with a 3D printer (30-50 units/day/printer) and a solution with a laser cutter (100 units/day/printer).  
  4 other prototypes are being developed to further increase the production capacity.

##### Material resources requested in the Fablabs
  * 3D printing (slow but can be distributed in other fablabs) + PLA/PETG
  * laser cutting machine (fast)
  * transparent A4 plastic sheets already cut and available
  * Plexiglas 3 mmm

Translated with www.DeepL.com/Translator (free version)

## Solutions ##

4 models have been validated by the medical officers of health and the management of the CHU Saint-Pierre hospital in Brussels with a first order of 1500 units to be produced. We share tutorials for the 4 of them here.

## 1 - Solution to the 3D printer

Inspired by the protective mask created by [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), we have developed a lighter version that is faster to produce due to time constraints.

![](./images/P1-summary.jpeg)

#### Hardware and tools

* Material :
  * a transparent A4 sheet
  * PETG or PLA coil
* Tools:
  * 3D printer
  * punch (5 mm or 6 mm holes)
* Production time: 30 min per mask

#### Tutorial

[Tutorial here](./PFC-Headband-Light-3DPrint/tuto-en.md)

## 2 - Laser Cutter Solution

We have had [this model from Thingiverse designed by LadyLibertyHK](http://thingiverse.com/thing:4159366) validated by the CHU Saint-Pierre hospital in Brussels.

![](./images/P2-summary.jpeg)


#### Hardware and tools

* Material
  * Plexiglas plate in 3 mm
  * transparent A4 sheet
* Tools:
  * laser cutting machine
  * one drill + 3.2 mm drill bit.
* Production time: 1 minute per mask

#### Tutorial

[Tutorial here](./PFC-Cap-LaserCut/tuto-en.md)

## 3 - Flexible strip solution with laser cutting machine

This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and slightly adapted thanks to feedback from the CHU Saint-Pierre hospital in Brussels. It is very quick to produce and less brittle than the rigid version.

![](./images/headband-flexible-general.png)

#### Hardware

* Material
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 1mm thick, 70 cm long minimum.
* Tools:
  * Laser cutting machine
  * Cutter
* Production time: 2 min/piece

#### Tutorial

The complete tutorial and the files for its realization [are available here](PFC-Headband-Flexible-LaserCut/tuto-en.md).

### 4 - Hygiene cap

simple solution from the Department of Stomatology and Maxillofacial Surgery of the St-Pierre University Hospital in Brussels and from the Fablab ULB.

![](./images/P4-Hygiene_cap.jpeg)

#### Material

* Material
  * A Hygiene cap
  * A sheet of transparent A4 plastic.
* Tools:
  * Stapler
* Production time: 1 min/piece

#### Tutorial

The complete tutorial [is available here](PFC-Hygiene-Cap/tuto-en.md).


## About Us

The [Fablab ULB](http://fablab-ulb.be/) (Université Libre de Bruxelles ULB, Belgium) is a multidisciplinary team of academics, researchers, technicians, students from different Faculties (Science, Architecture, Law, Brussels Polytechnic) and also artists, designers, makers and citizens. It is supported in its mission by various ULB research laboratories, namely the Frugal LAB (Faculty of Sciences), the Juris LAB (Faculty of Law) and the BEAMS (Ecole Polytechnique de Bruxelles).

This particular project was carried out in collaboration with the [Fab-C](https://www.fablab-charleroi.be) in Charleroi and the fablab [YourLab](https://yourlab.be/) in Andenne and supported by the Michel Crémer Foundation.
