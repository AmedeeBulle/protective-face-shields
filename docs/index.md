# Protective Face Shields

[For English click here](index-en.md)

### Fabrication de masques anti projections pour les hopitaux dans les fablabs

Une équipe **pluridisciplinaire** du Fablab ULB (Belgique) a développé des masques anti-projections en collaboration et à destination du CHU Saint-Pierre à Bruxelles. Nous sommes maintenant contactés par plusieurs hôpitaux de Belgique et à l'étranger. Nous sommes actuellement épaulés par le Fab-C à Charleroi et le fablab YourLab à Andenne, d'autres Fablabs rejoignent l'opération.

Voici plusieurs tutoriel ci-dessous pour reproduire ces masques dans les différents fablabs. Les 3 modèles sur ce site ont été validés par le CHU Saint-Pierre de Bruxelles.
Pour les makers experts, [voici notre Gitlab repository](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields) sur lequel vous trouverez tous les fichiers sources.


## News

[28 mars] Une nouvelle solution disponible avec une simple charlotte.

[26 mars] A mettre à jour.
100 masques (sol-1 3D printed) sont en production par Andreas Kopp pour l'hopital Wolfartsklinik Gräfelfing (Allemagne)
120 masques ont été produits et livrés au CPAS de Wavre  
50 masques pour l'hopital Erasme

[25 mars] 732 masques produits et livrés  (620 découpés au laser, 112 imprimés en 3D).  
50 protos réalisés (nouveau modèle en préparation).

[24 mars] 640 masques produits et livrés (540 découpés au laser, 100 imprimés en 3D).  
4 autres prototypes sont en cours de développement au Fablab ULB pour augmenter la capacité de production. Ils sont à ce jour validés par le CHU Saint-Pierre.
[RTBF JT de 13h et de 19h - 4'50"](https://www.rtbf.be/auvio/detail_jt-13h?id=2616606)

[23 mars] 140 masques ont été livrés au CHU Saint-Pierre à Bruxelles.

[22 mars 2020] 2 prototypes ont été validés par le CHU Saint-Pierre à Bruxelles.

## Contexte

##### Besoin général
  besoin urgent de plusieurs milliers de masques anti projections dans les hôpitaux Bruxellois. Estimation du nombre : 7500 unités sur Bruxelles.

##### Besoin confirmé :
  * Hôpital CHU Saint-Pierre à Bruxelles -> 1500 unités
  * Hopital Erasme (Bruxelles)
  * ISPPC - CHU Charleroi


##### Processus de validation [IMPORTANT]
  Chaque modèle doit être validé par un hôpital et un processus en 3 étapes avant d'être produit en grande quantité et de pouvoir être utilisé par un hôpital :
      1. un médecin de l'hôpital
      2. un médecin hygiéniste de l'hôpital
      3. la direction de l'hôpital

##### Prototypage et solutions validées :
  Le Fablab ULB a développé 2 solutions validées par le CHU Saint-Pierre BXL le 22 mars. Une solution à l'imprimante 3D (30-50 unités/jour/imprimante) et une solution à la découpeuse laser (100 unités/jour/imprimante).  
  4 autres prototypes sont en cours d'élaboration pour encore augmenter la capacité de production.

##### Ressources matériels sollicitées dans les Fablabs
  * impression 3D (lente mais on peut distribuer dans d'autres fablabs) + PLA/PETG
  * découpeuse laser (rapide)
  * feuilles plastiques A4 transparentes déjà découpées et disponibles
  * plexiglas 3 mmm

## Solutions

4 modèles ont été validés par les médecins hygiénistes et la direction de l’hôpital CHU Saint-Pierre à Bruxelles avec une première commande de 1500 unités à produire. Nous les partageons ici ci-dessous.

### 1 - Solution à l'imprimante 3D

Inspiré du masque de protection créé par [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), nous avons développé une version plus légère et plus rapide à produire vu les contraintes de temps.

![](./images/P1-summary.jpeg)

#### Matériel et outils

* Matériel :
  * une feuille A4 transparente
  * bobine de PETG ou de PLA
* Outils :
  * imprimante 3D
  * perforatrice (trous de 5 mm ou 6 mm)
* Temps de production : 30 min par masque

#### Tutoriel

[Tutoriel ici](./PFC-Headband-Light-3DPrint/tuto-fr.md)

### 2 - Solution à la découpeuse laser

Nous avons fait valider [ce modèle de chez Thingiverse conçu par LadyLibertyHK](http://thingiverse.com/thing:4159366) par l'hôpital CHU Saint-Pierre à Bruxelles.

![](./images/P2-summary.jpeg)


#### Matériel et outils

* Matériel
  * plaque de plexiglas en 3 mm
  * feuille A4 transparente
* Outils :
  * découpeuse laser
  * une foreuse + mêche 3.2 mm
* Temps de production : 1 minute par masque

#### Tutoriel

[Tutoriel ici](./PFC-Cap-LaserCut/tuto-fr.md)

### 3 - Solution en bandeau flexible à la découpeuse laser

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, du service d'anesthésie de l'hôpital d'Ixelles, et légèrement adaptée grâce aux retours de l’hôpital CHU Saint-Pierre à Bruxelles. Elle est très rapide à produire et moins cassante que la version rigide.

![](./images/headband-flexible-general.png)

#### Matériel

* Matériel
  * Une feuille de PVC A4 transparente.
  * Une feuille de Priplak® (polypropylene), d’épaisseur 1mm, de 70 cm de longueur minimum.
* Outils :
  * Découpeuse laser
  * Cutter
* Temps de production : 2 min/pièce

#### Tutoriel

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Headband-Flexible-LaserCut/tuto-fr.md).

### 4 - Solution charlotte

Une simple solution issue du service de Stomatologie et chirurgie maxillo-faciale du CHU St-Pierre à Bruxelles et du Fablab ULB

![](./images/P4-Hygiene_cap.jpeg)

#### Matériel

* Matériel
  * Une charlotte
  * Une feuille de plastique A4 transparente.
* Outils :
  * Agrafeuse
* Temps de production : 1 min/pièce

#### Tutoriel

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Hygiene-Cap/tuto-fr.md).

## Commandes et proposition d'aide

Pour toutes demandes de masques ou proposition d'aide, veuillez contacter le Fablab ULB à l'adresse suivante : contact(at)fablab-ulb.be

## About Us

Le [Fablab ULB](http://fablab-ulb.be/) est une équipe pluridisciplinaire d'académiques, chercheurs, techniciens, étudiants de différentes Facultés (Sciences, Architecture, Droit, Ecole Polytechnique de Bruxelles) et aussi d'artistes, designers, makers et citoyens. Il est appuyé dans sa mission par différents laboratoires de recherche de l'ULB à savoir le Frugal LAB (Faculté des Sciences), le Juris LAB (Faculté de Droit) et le BEAMS (Ecole Polytechnique de Bruxelles).

Ce projet particulier a été résalisé en collaboration avec le [Fab-C](https://www.fablab-charleroi.be) de Charleroi et le fablab [YourLab](https://yourlab.be/) d'Andenne et soutenu par la fondation Michel Crémer.
