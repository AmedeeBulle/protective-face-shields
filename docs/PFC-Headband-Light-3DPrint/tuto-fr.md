# Solution à l'imprimante 3D

Inspiré du masque de protection créé par [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), nous avons développé une version plus légère et plus rapide à produire vu les contraintes de temps.

![](../images/P1-summary.jpeg)

## Matériel

* Matériel :
  * une feuille A4 transparente
  * bobine de PETG ou de PLA
* Outils :
  * imprimante 3D
  * perforatrice (trous de 5 mm ou 6 mm)
* Temps de production : 30 min par masque

## Processus de fabrication

### Etape 1 - Impression de la structure

Téléchargez le modèle ci-dessous conçu par Nicolas De Coster et imprimez le en PETG ou en PLA (30 min).

![](../images/P1-3Dprint.jpeg)

Pour les perforatrices de 6 mm (standard) :

* [modèle 1x](../files/anti_projection_6mmPin.stl)

Pour les perforatrices de 5 mm :

* [modèle 1x](../files/anti_projection_5mmPin.stl)

### Etape 2 - Trouer la feuille A4 transparente

A l'aide d'une perforatrice, faire 3 trous à une distance de 4C cm du bord long de la feuille transparente.

[Pour le tutoriel, nous avons utilisé une feuille de couleur]

![](../PFC-Headband-Light-3DPrint/images/P1-feuille3trous.jpeg)

#### 2 trous latéraux

Faire 2 trous de part et d'autre du transparent.  
Attention à bien aligner le bord de la feuille sur le repère central de la perforatrice pour bien positionner le trou (voir photo ci-dessous).

![](../PFC-Headband-Light-3DPrint/images/P1-trou1-2.jpeg)


#### le trou central

plier la feuille en 2 et perforer au centre sur le pli à hauteur des 2 autres trous.

![](../PFC-Headband-Light-3DPrint/images/P1-trou_central.jpeg)

### Etape 3 - Elastique

Assembler l'élastique

![](../PFC-Headband-Light-3DPrint/images/P1-elastique.jpeg)

### Etape 4 - Assembler

l'assemblage se fait à l'hopital.

## Pour aller plus loin et plus vite

Il est aussi possible de lancer l'impression de plusieurs modèles empilés. Cela permet de laisser tourner les imprimantes pendant de très longues heures.

Pour voir comment faire, rendez-vous sur notre [Gitlab repository](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/tree/master/PFC-Headband-Light-3DPrint)

![](../PFC-Headband-Light-3DPrint/images/P1-3Dprint-stacked.jpeg)

## Livraison

![](../PFC-Headband-Light-3DPrint/images/P1-delivery.jpeg)

## Auteurs

Tutoriel réalisé par Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgique) et Nicolas De Coster (IRM & Fablab ULB).  
