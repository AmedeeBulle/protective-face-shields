# Solution at the 3D printer

Inspired by the protective face mask created by [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), we have developed a lighter version that is faster to produce due to time constraints.

![](../images/P1-summary.jpeg)

## Hardware

* Material :
  * a transparent A4 sheet
  * PETG or PLA coil
* Tools:
  * 3D printer
  * punch (5 mm or 6 mm holes)
* Production time: 30 min per mask

## Manufacturing process

### Step 1 - Printing the structure

Download the template below designed by Nicolas De Coster [MIT license](https://opensource.org/licenses/MIT) and print it in PETG or PLA (30 min).

![](../images/P1-3Dprint.jpeg)

For 6 mm punches (standard) :

* [model 1x](../wires/anti_projection_6mmPin.stl)

For 5 mm punches :

* [1x model](../wires/anti_projection_5mmPin.stl)

### Step 2 - Hole the transparent A4 sheet of paper

Using a punch, make 3 holes at a distance of 4 cm from the long edge of the transparent sheet.

[For the tutorial, we used a colored sheet]

![](../PFC-Headband-Light-3DPrint/images/P1-sheet3holes.jpeg)

#### 2 side holes

Make 2 holes on both sides of the transparency.  
Be careful to align the edge of the sheet with the central mark of the punch to position the hole (see photo below).

![](../PFC-Headband-Light-3DPrint/images/P1-hole1-2.jpeg)


#### the center hole

Fold the sheet in half and perforate in the centre of the fold at the level of the 2 other holes.

![](../PFC-Headband-Light-3DPrint/images/P1-central_hole.jpeg)

### Step 3 - Elastic

Assembling the elastic band

![](../PFC-Headband-Light-3DPrint/images/P1-elastic.jpeg)

### Step 4 - Assemble

the assembly is done at the hospital.

## To go further and faster ##

It is also possible to print multiple stacked templates. This allows the printers to run for very long hours.

To see how to do this, go on our [Gitlab repository](https://gitlab.com/fablab-ulb/projects/coronavirus/protective-face-shields/-/tree/master/PFC-Headband-Light-3DPrint)

![](../PFC-Headband-Light-3DPrint/images/P1-3Dprint-stacked.jpeg)

## Delivery

![](../PFC-Headband-Light-3DPrint/images/P1-delivery.jpeg)

## Authors

Tutorial written by Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgium) and Nicolas De Coster (IRM & Fablab ULB).  
