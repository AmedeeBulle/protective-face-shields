# Solution à la découpeuse laser

Nous avons fait valider [ce modèle de chez Thingiverse conçu par LadyLibertyHK](http://thingiverse.com/thing:4159366) par l'hôpital CHU Saint-Pierre à Bruxelles.

![](../images/P2-summary.jpeg)

## Matériel

* Matériel
  * plaque de plexiglas en 3 mm
  * feuille A4 transparente
* Outils :
  * découpeuse laser
  * une foreuse + mêche 3.2 mm
* Temps de production : 1 minute par masque

## Processus de fabrication

### Etape 1 - Découpe de la structure

Téléchargez les fichiers SVG optimisés pour une plaque 100x60cm ci-dessous et découpez-les à la découpeuse laser.

* [SVG 30 pièces (100x60cm)](files/Mask_30pcs_overcut.svg)

![](../PFC-Cap-LaserCut/images/P2-30Masks.png)

### Etape 2 - Trouer la feuille A4 transparente

A l'aide d'une foreuse et d'une mèche 3.2 mm, trouer une pile de 10 feuilles A4 transparentes selon [le patron suivant](files/PCF-Cap-gabarit-transparent.DXF).

![](../PFC-Cap-LaserCut/images/P2-transparent-trous.jpeg)
![](../PFC-Cap-LaserCut/images/cap-gabarit.png)

### Etape 3 - Assembler

l'assemblage se fait à l'hôpital.

## Auteurs et remerciements

Tutoriel réalisé par Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgique). Merci à toute l'équipe pour votre contribution et vos photos.
