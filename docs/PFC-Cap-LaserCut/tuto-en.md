# Solution at the laser cutter

We have had [this model from Thingiverse designed by LadyLibertyHK](http://thingiverse.com/thing:4159366) validated by the CHU Saint-Pierre hospital in Brussels.

![](../images/P2-summary.jpeg)

## Hardware

* Material
  * Plexiglas plate in 3 mm
  * transparent A4 sheet
* Tools:
  * laser cutting machine
  * one drill + 3.2 mm drill bit.
* Production time: 1 minute per mask

## Manufacturing process

### Step 1 - Cutting out the structure

Download the SVG files optimized for a 100x60cm plate below and cut them with the laser cutter.

* [SVG 30 pieces (100x60cm)](files/Mask_30pcs_overcut.svg)

![](../PFC-Cap-LaserCut/images/P2-30Masks.png)

### Step 2 - Punch holes in the transparent A4 sheet of paper

Using a drill and a 3.2 mm drill bit, drill holes in a stack of 10 transparent A4 sheets according to [the following pattern](files/PCF-Cap-gabarit-transparent.DXF).

![](../PFC-Cap-LaserCut/images/P2-transparent-trous.jpeg)
![](../PFC-Cap-LaserCut/images/cap-gabarit.png)

### Step 3 - Assemble

the assembly is done at the hospital.

## Authors and Acknowledgements

Tutorial written by Denis Terwagne, (Frugal LAB & Fablab ULB, Université Libre de Bruxelles, Belgium). Thanks to the whole team for your contribution and your pictures.
