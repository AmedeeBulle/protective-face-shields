# Fabrication de masques anti projections pour les hopitaux dans les fablabs

Le Fablab ULB (Belgique) a développé et produit des masques anti-projections à destination du CHU Saint-Pierre à Bruxelles.

Nous sommes maintenant contactés par plusieurs hôpitaux de Belgique.

Nous sommes actuellement épaulés par le Fab-C à Charleroi et le fablab YourLab à Andenne, d'autres Fablabs vont se joindre à l'aventure.

Voici un tutoriel pour reproduire ces masques dans les différents fablabs.

## News

* [24 mars]
  * 640 masques produits et livrés (540 découpés au laser, 100 imprimés en 3D).
  * 4 autres prototypes sont en cours de développement au Fablab ULB pour augmenter la capacité de production.
  * [RTBF JT de 13h et de 19h - 4'50"](https://www.rtbf.be/auvio/detail_jt-13h?id=2616606)
* [23 mars]
  * 140 masques ont été livrés au CHU Saint-Pierre à Bruxelles.
* [22 mars 2020]
  * 2 prototypes ont été validés par le CHU Saint-Pierre à Bruxelles.

## Contexte

* Besoin général
  * besoin urgent de plusieurs milliers de masques anti-projections dans les hôpitaux Bruxellois. Estimation du nombre : 7500 unités sur Bruxelles.

* Besoin confirmé :
   * Hôpital CHU Saint-Pierre à Bruxelles -> 1500 unités

* Processus de validation [IMPORTANT]
  * Chaque modèle doit être validé par un hôpital et un processus en 3 étapes avant d'être produit en grande quantité et de pouvoir être utilisé par un hôpital :
      1. un médecin de l'hôpital
      2. un médecin hygiéniste de l'hôpital
      3. la direction de l'hôpital

* Prototypage et solutions validées :
  * Le Fablab ULB a développé 2 solutions validées par le CHU Saint-Pierre BXL le 22 mars. Une solution à l'imprimante 3D (30-50 unités/jour/imprimante) et une solution à la découpeuse laser (100 unités/jour/imprimante).
  * 4 autres prototypes sont en cours d'élaboration pour encore augmenter la capacité de production.

* Ressources dans les Fablabs
  * impression 3D (lente mais on peut distribuer dans d'autres fablabs)
  * découpeuse laser (rapide)
  * utiliser des feuilles plastiques A4 transparentes déjà découpées et disponibles

## Solutions

Deux modèles ont été validés par les médecins hygiénistes et la direction de l’hôpital CHU Saint-Pierre à Bruxelles -> 1500 unités à produire.

* [1 - Solution à l'imprimante 3D (en orange ci-dessous)](#1-solution-à-limprimante-3d) (30 min/masque)
* [2 - Solution à la découpeuse laser (en jaune ci-dessous)](#2-solution-à-la-découpeuse-laser) (1 min/masque)
* [3 - Solution en bandeau flexible à la découpeuse laser](#3-solution-en-bandeau-flexible-à-la-découpeuse-laser) (2 min/masque)

![](./docs/images/2Masks.png)


### 1 - Solution à l'imprimante 3D

Inspiré du masque de protection créé par [Prusa](https://www.prusaprinters.org/prints/25857-prusa-protective-face-shield-rc2/files), nous avons développé une version plus légère et plus rapide à produire vu les contraintes de temps.

![](./docs/images/P1-summary.jpeg)

#### Matériel

* Matériel :
  * une feuille A4 transparente
  * bobine de PETG ou de PLA
* Outils :
  * imprimante 3D
  * perforatrice (trous de 5 mm ou 6 mm)
* Temps de production : 30 min par masque

#### Processus de fabrication

##### Etape 1 - Impression de la structure

Téléchargez le modèle ci-dessous conçu par Nicolas De Coster et imprimez le en PETG ou en PLA (30 min).

![](./PFC-Headband-Light-3DPrint/images/P1-3Dprint.jpeg)

Pour les perforatrices de 6 mm (standard)
* [modèle 1x](./PFC-Headband-Light-3DPrint/anti_projection_6mmPin.stl)

Pour les perforatrices de 5 mm
* [modèle 1x](./PFC-Headband-Light-3DPrint/anti_projection_5mmPin.stl)

##### Etape 2 - Trouer la feuille A4 transparente

A l'aide d'une perforatrice, faire 3 trous à une distance de 4C cm du bord long de la feuille transparente.

[Pour le tutoriel, nous avons utilisé une feuille de couleur]

![](./PFC-Headband-Light-3DPrint/images/P1-feuille3trous.jpeg)

###### 2 trous latéraux

Faire 2 trous de part et d'autre du transparent.  
Attention à bien aligner le bord de la feuille sur le repère central de la perforatrice pour bien positionner le trou (voir photo ci-dessous).

![](./PFC-Headband-Light-3DPrint/images/P1-trou1-2.jpeg)


###### le trou central

plier la feuille en 2 et perforer au centre sur le pli à hauteur des 2 autres trous.

![](./PFC-Headband-Light-3DPrint/images/P1-trou_central.jpeg)

##### Etape 3 - Elastique

Assembler l'élastique

![](./PFC-Headband-Light-3DPrint/images/P1-elastique.jpeg)

##### Etape 4 - Assembler

l'assemblage se fait à l'hopital.

### 2 - Solution à la découpeuse laser

Nous avons fait valider [ce modèle de chez Thingiverse conçu par DrBioPhysics](http://thingiverse.com/thing:4155735) par l'hôpital CHU Saint-Pierre à Bruxelles.

![](./docs/images/P2-summary.jpeg)

#### Matériel

* Matériel
  * plaque de plexiglas en 3 mm
  * feuille A4 transparente
* Outils :
  * découpeuse laser
  * une foreuse + mêche 3.2 mm
* Temps de production : 1 minute par masque

#### Processus de fabrication

##### Etape 1 - Découpe de la structure

Téléchargez les fichiers SVG optimisés pour une plaque 100x60cm ci-dessous et découpez-les à la découpeuse laser.
* [SVG 30 pièces (100x60cm)](./PFC-Cap-LaserCut/Mask_30pcs.svg)


![](./PFC-Cap-LaserCut/images/P2-36Masks.png)

##### Etape 2 - Trouer la feuille A4 transparente

A l'aide d'une foreuse et d'une mèche 3.2 mm, trouer une pile de 10 feuilles A4 transparentes selon [le patron suivant](). [FICHIER A METTRE A JOUR]

![](./PFC-Cap-LaserCut/images/P2-transparent-trous.jpeg)

##### Etape 3 - Assembler

l'assemblage se fait à l'hôpital.

### 3 - Solution en bandeau flexible à la découpeuse laser

Cette solution a été inspirée d’un design de Dr Carmen Ionescu, , du service d'anesthésie de l'hopital d'Ixelles, et légèrement adaptée grâce aux retours de l’hôpital CHU Saint-Pierre à Bruxelles. Elle est très rapide à produire et moins cassante que la version rigide.

![](./docs/images/headband-flexible-general.png)

Le tutoriel complet ansi que les fichiers pour sa réalisation [sont disponibles ici](PFC-Headband-Flexible-LaserCut/README.md).

## Auteurs

Tutoriel réalisé par Denis Terwagne, Frugal LAB & Fablab ULB, Université Libre de Bruxelles (Belgique)  
Merci à toute l'équipe pour votre contribution et vos photos.
